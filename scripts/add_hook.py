# -*- coding: utf-8 -*-
from argparse import ArgumentParser, Namespace, SUPPRESS, ArgumentError, RawTextHelpFormatter
from base64 import b64decode
from binascii import Error
from http.client import HTTPResponse
from json import loads
from json.decoder import JSONDecodeError
from logging import Logger, ERROR, Formatter, getLogger, FileHandler, shutdown, StreamHandler
from os.path import relpath
from pathlib import Path
from sys import stdout, platform
from typing import Any, NamedTuple
from urllib.error import URLError, HTTPError
from urllib.request import Request, urlopen

PROJECT_ID: int = 58401192
FILE_NAME: str = "scripts%2Finspect_yaml.py"

FMT: str = "%(asctime)s %(levelname)s::%(funcName)s::%(lineno)d -- %(message)s"
DATEFMT: str = "%d-%m-%Y %H:%M:%S"

STOP: str = "Нажмите любую клавишу для завершения скрипта ...\n"
NAME: str = Path(__file__).name

PRE_COMMIT_CONFIG: dict[str, dict | list[dict]] = {
    "repos": [
        {
            "repo": "local",
            "hooks": [
                {
                    "id": "inspect-yaml",
                    "name": "inspect yaml file",
                    "language": "python"
                }
            ]
        }
    ]
}


class CustomHTTPRequest:
    def __init__(
            self, *,
            scheme: str | None = None,
            web_hook: str | None = None,
            host: str | None = None,
            params: tuple[tuple[str, str], ...] | None = None):
        if scheme is None:
            scheme: str = "https"

        self._scheme: str = scheme
        self._web_hook: str = web_hook
        self._host: str = host
        self._params: tuple[tuple[str, str], ...] = params

    def _get_params(self) -> str:
        if self._params is None:
            return ""
        else:
            _params = "&".join([f"{name}={value}" for name, value in self._params])
            return f"?{_params}"

    @property
    def url(self) -> str:
        return (
            f"{self._scheme.lower().strip('/')}://"
            f"{self._host.strip('/')}/"
            f"{self._web_hook.rstrip('/')}"
            f"{self._get_params()}")

    def http_response(self) -> HTTPResponse:
        try:
            request: Request = Request(
                self.url,
                b'',
                dict(),
                self._host,
                False,
                "GET")
            urlopen(request)
            return urlopen(request)

        except HTTPError as e:
            logger.error(f"Ошибка HTTP {e.code}, {e.reason}\nЗаголовки:\n{e.headers}\nСсылка {e.url}")
            input(STOP)
            exit(2)

        except URLError as e:
            logger.error(f"Ошибка {e.strerror}, {e.reason}")
            input(STOP)
            exit(3)

        except OSError as e:
            logger.error(f"Ошибка {e.__class__.__name__}: {e.strerror}")
            input(STOP)
            exit(1)


class CustomHTTPResponseChunked:
    def __init__(
            self,
            path: str | Path,
            http_response: HTTPResponse, *,
            chunk: int = 4096):
        self._path: Path = Path(path).resolve()
        self._http_response: HTTPResponse = http_response
        self._chunk: int = chunk
        self._response_dict: dict[str, Any] = dict()
        self.get_response()

    def decode_base64(self) -> bytes:
        try:
            _content: bytes = self._response_dict.get("content")
            return b64decode(_content)

        except Error as e:
            logger.error(f"Ошибка декодирования Base64 {str(e)}")
            input(STOP)
            exit(4)

        except UnicodeDecodeError as e:
            logger.error(
                f"Ошибка декодирования {e.encoding} {e.reason}\n"
                f"Строка\n{e.object}\n"
                f"Некорректная подстрока {e.object[e.start:e.end]}")
            input(STOP)
            exit(5)

        except UnicodeError as e:
            logger.error(f"Ошибка {e.__class__.__name__} {str(e)}")
            input(STOP)
            exit(6)

        except OSError as e:
            logger.error(f"Ошибка {e.__class__.__name__}: {e.strerror}")
            input(STOP)
            exit(1)

    def get_response(self):
        self._path.touch(exist_ok=True)

        try:
            with open(self._path, mode="w+b") as fb:
                while chunk := self._http_response.read(self._chunk):
                    fb.write(chunk)
                fb.seek(0)
                self._response_dict = loads(fb.read())

        except JSONDecodeError as e:
            logger.error(f"Ошибка декодирования {e.doc}\nПозиция {e.lineno}:{e.colno}")
            input(STOP)
            exit(7)

        except HTTPError as e:
            logger.error(f"Ошибка HTTP {e.code}, {e.reason}\nЗаголовки:\n{e.headers}\nСсылка {e.url}")
            input(STOP)
            exit(2)

        except URLError as e:
            logger.error(f"Ошибка {e.strerror}, {e.reason}")
            input(STOP)
            exit(3)

        except FileNotFoundError:
            logger.error(f"Файл {self._path} не найден")
            input(STOP)
            exit(8)

        except PermissionError:
            logger.error(f"Недостаточно прав для записи в файл {self._path}")
            input(STOP)
            exit(9)

        except RuntimeError:
            logger.error(f"Истекло время записи в файл {self._path}")
            input(STOP)
            exit(10)

        except OSError as e:
            logger.error(f"Ошибка {e.__class__.__name__}: {e.strerror}")
            input(STOP)
            exit(1)


class ContentGitItem(NamedTuple):
    project_id: int | str
    file_name: str
    path: str | Path

    @property
    def _custom_http_request(self):
        scheme: str = "https"
        web_hook: str = f"api/v4/projects/{self.project_id}/repository/files/{self.file_name}"
        host: str = "gitlab.com"
        params: tuple[tuple[str, str], ...] = (("ref", "main"),)

        return CustomHTTPRequest(
            scheme=scheme,
            web_hook=web_hook,
            host=host,
            params=params)

    def download(self):
        _http_response: HTTPResponse = self._custom_http_request.http_response()
        _chunked: CustomHTTPResponseChunked = CustomHTTPResponseChunked(self.path, _http_response)
        _content: bytes = _chunked.decode_base64()

        try:
            with open(self.path, "w+b") as f:
                f.write(_content)

        except FileNotFoundError:
            logger.error(f"Файл {self.path} не найден")
            input(STOP)
            exit(8)

        except PermissionError:
            logger.error(f"Недостаточно прав для записи в файл {self.path}")
            input(STOP)
            exit(9)

        except RuntimeError:
            logger.error(f"Истекло время записи в файл {self.path}")
            input(STOP)
            exit(10)

        except OSError as e:
            logger.error(f"Ошибка {e.__class__.__name__}: {e.strerror}")
            input(STOP)
            exit(1)


def get_script():
    _home: Path = Path.home()
    _pycharm_projects: Path = _home.joinpath("PycharmProjects")
    _intellij_projects: Path = _home.joinpath("IdeaProjects")

    if _pycharm_projects.exists():
        _path_script: Path = _pycharm_projects.joinpath("inspect_yaml.py")

    elif _intellij_projects.exists():
        _path_script: Path = _intellij_projects.joinpath("inspect_yaml.py")

    else:
        _path_script: Path = _home.joinpath("inspect_yaml.py")

    if not _path_script.exists():
        content_git_item: ContentGitItem = ContentGitItem(
            PROJECT_ID,
            FILE_NAME,
            _path_script
        )
        content_git_item.download()

    return _path_script


def delete_empty_log():
    _add_hook: Path = Path.cwd().joinpath("add_hook.log")

    with open(_add_hook, "r", encoding="utf-8") as f:
        f.seek(0)

        _is_empty: bool = not f.read().strip()

    if _is_empty:
        _add_hook.unlink(missing_ok=True)


class IDEProject:
    def __init__(self, project_root: str | Path, script: str | Path):
        self._project_root: Path = Path(project_root).resolve()
        self._script: Path = Path(script).resolve()
        self._validate()

    def _validate(self):
        if not self._project_root.exists():
            logger.error(f"Директория {self._project_root} не найдена")
            input(STOP)
            exit(8)

        elif not self._project_root.is_dir():
            logger.error(f"Путь {self._project_root} указывает не на директорию")
            input(STOP)
            exit(11)

        elif not self._project_root.joinpath(".git").exists():
            logger.error(f"Директория {self._project_root}/.git не найдена, т.е. проект не связан с git")
            input(STOP)
            exit(12)

    @property
    def pre_commit(self) -> bytes:
        if platform.startswith("win"):
            shebang: str = "#!/usr/bin/bash"
            python_executable: str = "python"

        else:
            shebang: str = "#/bin/bash"
            python_executable: str = "python3"

        path: str = relpath(self._script, self._project_root)
        _pre_commit: str = f"{shebang}\n\n{python_executable} {path}"

        return _pre_commit.encode("utf-8")

    def generate_hook(self):
        hook: Path = self._project_root.joinpath(".git/hooks/pre-commit")
        hook.touch(mode=0o755, exist_ok=True)

        try:
            with open(hook, "w+b") as fb:
                fb.write(self.pre_commit)

        except FileNotFoundError:
            logger.error(f"Файл {hook} не найден")
            input(STOP)
            exit(8)

        except PermissionError:
            logger.error(f"Недостаточно прав для записи в файл {hook}")
            input(STOP)
            exit(9)

        except RuntimeError:
            logger.error(f"Истекло время записи в файл {hook}")
            input(STOP)
            exit(10)

        except OSError as e:
            logger.error(f"Ошибка {e.__class__.__name__}: {e.strerror}")
            input(STOP)
            exit(1)

    def generate_pre_commit_config(self):
        path: Path = Path(self._project_root).joinpath(".pre-commit-config.yaml")

        try:
            with open(path, "w", encoding="utf-8") as f:
                safe_dump(
                    PRE_COMMIT_CONFIG,
                    f,
                    indent=2,
                    allow_unicode=True,
                    encoding="utf-8",
                    sort_keys=False)

        except MarkedYAMLError as e:
            logger.error(f"Ошибка YAML: {e.problem_mark.line + 1}:{e.problem_mark.column + 1})")
            input(STOP)
            exit(13)

        except OSError as e:
            logger.error(f"Ошибка {e.__class__.__name__}: {e.strerror}")
            input(STOP)
            exit(1)


def parse():
    arg_parser: ArgumentParser = ArgumentParser(
        prog="add_hook.py",
        usage=f"py {NAME} [ -h/--help | -v/--version ] <PROJECT_ROOT>",
        description="Добавление хука git pre-commit",
        formatter_class=RawTextHelpFormatter,
        epilog=(
            "Примеры:\n"
            "py .\\add_hook.py ..\\SigFW\n"
            "python ..\\scripts\\scripts\\add_hook.py .\\Protei_MME"),
        add_help=False,
        allow_abbrev=False,
        exit_on_error=False
    )

    arg_parser.add_argument(
        action="store",
        type=str,
        default=None,
        help="Путь до проекта git. Может быть как абсолютным, так и относительным.",
        dest="project_root"
    )

    arg_parser.add_argument(
        "-v", "--version",
        action="version",
        version="1.0.0",
        help="Показать версию скрипта и завершить работу"
    )

    arg_parser.add_argument(
        "-h", "--help",
        action="help",
        default=SUPPRESS,
        help="Показать справку и завершить работу"
    )

    try:
        args: Namespace = arg_parser.parse_args()

        if hasattr(args, "help") or hasattr(args, "version"):
            input(STOP)
            return

    except ArgumentError as exc:
        print(f"{exc.__class__.__name__}, {exc.argument_name}\n{exc.message}")
        input(STOP)
        exit(14)

    except KeyboardInterrupt:
        input("Работа скрипта остановлена пользователем ...")
        exit(100)

    project_root: str = getattr(args, "project_root")
    script: Path = get_script()
    ide_project: IDEProject = IDEProject(project_root, script)
    ide_project.generate_hook()
    ide_project.generate_pre_commit_config()


if __name__ == '__main__':
    file_formatter: Formatter = Formatter(fmt=FMT, datefmt=DATEFMT, style="%", validate=True)
    stream_formatter: Formatter = Formatter(fmt="%(message)s", style="%", validate=True)

    file_handler: FileHandler = FileHandler(filename="add_hook.log", mode="a", encoding="utf-8")
    file_handler.setLevel(ERROR)
    file_handler.setFormatter(file_formatter)

    stream_handler: StreamHandler = StreamHandler(stdout)
    stream_handler.setLevel(ERROR)
    stream_handler.setFormatter(stream_formatter)

    logger: Logger = getLogger(Path(__file__).stem)
    logger.setLevel(ERROR)
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    try:
        from yaml import safe_dump, MarkedYAMLError

    except (ModuleNotFoundError, ImportError):
        try:
            from subprocess import run, CalledProcessError

            run(["python", "-m", "pip", "install", "pyyaml"], check=True)

        except CalledProcessError as err:
            print(f"Ошибка выполнения команды {err.cmd}, {err.returncode}")
            input(STOP)
            exit(15)

    finally:
        from yaml import safe_dump, MarkedYAMLError

    parse()
    shutdown()
    delete_empty_log()
