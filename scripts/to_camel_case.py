# -*- coding: utf-8 -*-
from argparse import ArgumentError, ArgumentParser, BooleanOptionalAction, Namespace, RawTextHelpFormatter, SUPPRESS
from glob import iglob
from pathlib import Path
from re import Pattern, compile, Match, match, sub, finditer

from stringcase import camelcase

_PATTERN_NAME: Pattern = compile(r"^\|\s(\**)(<a name=\"[^\"]*\">)?(?:\\\[)*([^]<\\\s]*)(?:\\])*(</a>)?(.*)$")
_PATTERN_SECTION: Pattern = compile(r"^(.*)\\\[([^\\\]]+)\\?](.*)$")

NAME: str = Path(__file__).name
STOP: str = "\nНажмите любую клавишу для завершения скрипта ...\n"
_SUFFIXES: tuple[str, ...] = (".md", "adoc")


def repl_name(_match: Match):
    groups: list[str] = []

    for group in _match.groups():
        if group is None:
            groups.append("")
        else:
            groups.append(group)

    _name: str = groups[2]

    if "_" in _name or _name.isupper():
        groups[2] = _name.lower()

    return f"| {groups[0]}{groups[1]}{camelcase(groups[2])}{groups[3]}{groups[4]}"


def repl_section(_match: Match):
    groups: list[str] = []

    for group in _match.groups():
        if group is None:
            groups.append("")
        else:
            groups.append(group)

    _name: str = groups[1]

    if "_" in _name or _name.isupper():
        groups[1] = _name.lower()

    return f"{groups[0]}{camelcase(groups[1])}{groups[2]}"


def convert_to_camel_case(path: str | Path):
    with open(path, "r", encoding="utf-8", errors="ignore") as fr:
        content: list[str] = fr.readlines()

    for index, line in enumerate(content):
        if line.startswith("|"):
            _m: Match = match(_PATTERN_NAME, line)

            if _m:
                line: str = sub(_PATTERN_NAME, repl_name, line)
                content[index] = line

        if r"\\\[" in line:
            for _m in finditer(_PATTERN_SECTION, line):
                line: str = sub(_PATTERN_SECTION, repl_section, line)
            content[index] = line

    with open(path, "w", encoding="utf-8", errors="ignore") as fw:
        fw.writelines(content)

    print(f"Файл {path} обработан")


def parse():
    arg_parser: ArgumentParser = ArgumentParser(
        prog="list_files",
        usage=f"py {NAME} "
              f"[ -h/--help | -v/--version ] "
              f"[ -d/--dir <DIRNAME> ] "
              f"[ -f/--files <FILE> ] "
              f"[ --recursive/--no-recursive ]",
        description="Вывод файлов в директории",
        epilog="",
        add_help=False,
        allow_abbrev=False,
        exit_on_error=False,
        formatter_class=RawTextHelpFormatter
    )
    arg_parser.add_argument(
        "-d", "--dir",
        action="store",
        default=SUPPRESS,
        required=False,
        help="Директория, в которой необходимо обработать файлы.",
        dest="directory"
    )

    arg_parser.add_argument(
        "-f", "--files",
        action="extend",
        default=SUPPRESS,
        required=False,
        help="Перечень файлов, которые необходимо обработать.\nМожет использоваться несколько раз.",
        dest="files"
    )

    arg_parser.add_argument(
        "--recursive",
        action=BooleanOptionalAction,
        default=True,
        required=False,
        help="Флаг рекурсивного поиска файлов.\nПо умолчанию: True.",
        dest="recursive"
    )

    arg_parser.add_argument(
        "-v", "--version",
        action="version",
        version="1.0.0",
        help="Показать версию скрипта и завершить работу."
    )

    arg_parser.add_argument(
        "-h", "--help",
        action="help",
        default=SUPPRESS,
        help="Показать справку и завершить работу."
    )

    try:
        args: Namespace = arg_parser.parse_args()

        if hasattr(args, "help") or hasattr(args, "version"):
            input(STOP)
            exit(0)

        elif not (hasattr(args, "files") or hasattr(args, "directory")):
            print(f"Не введены ни файлы, ни директория.")
            input(STOP)
            exit(0)

    except ArgumentError as exc:
        print(f"\n{exc.__class__.__name__}, {exc.argument_name}\n{exc.message}")
        input(STOP)
        exit(1)

    except KeyboardInterrupt:
        input("\nРабота скрипта остановлена пользователем ...")
        exit(2)

    else:
        return args


def get_files(ns: Namespace):
    directory: str | None = getattr(ns, "directory", None)
    files: list[str] = getattr(ns, "files", list())
    recursive: bool = getattr(ns, "recursive", True)

    def _join(value: str):
        return directory.joinpath(value)

    _files: list[Path] = [Path(_file).resolve() for _file in files]

    if directory is not None:
        directory: Path = Path(directory).resolve()

        _files.extend(
            [_join(item)
             for item in iglob("**/*", root_dir=directory, recursive=recursive)
             if _join(item).is_file() and _join(item).suffix in _SUFFIXES])

    return _files


if __name__ == '__main__':
    user_input: Namespace = parse()

    for file in get_files(user_input):
        convert_to_camel_case(file)
