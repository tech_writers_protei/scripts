# -*- coding: utf-8 -*-
from argparse import ArgumentError, ArgumentParser, Namespace, ONE_OR_MORE, SUPPRESS
from os import scandir
from pathlib import Path
from re import DOTALL, MULTILINE, Pattern, compile, sub
from typing import Iterable

FOREIGN_OBJECT: Pattern = compile("<foreignObject.*?</foreignObject>", DOTALL | MULTILINE)
TEXT: Pattern = compile(
    "<a\s?transform=\"translate(0,-5)\"\s?xlink:href=\"https://www.drawio.com/doc/faq/svg-export-text-problems"
    "\".*?</a>", DOTALL | MULTILINE)
FEATURES: Pattern = compile(
    "<g>\s?<g\s?requiredFeatures=\"http://www.w3.org/TR/SVG11/feature#Extensibility\"/>\s?</g>", DOTALL | MULTILINE)
NAME: str = Path(__file__).name
PRESS_ENTER_KEY: str = "\nНажмите ENTER, чтобы завершить работу скрипта ..."


def repair_svg(path: str | Path):
    try:
        with open(path, "rb") as fr:
            svg: str = fr.read().decode()

        svg: str = sub(TEXT, "", svg)
        svg: str = sub(FOREIGN_OBJECT, "", svg)
        svg: str = sub(FEATURES, "", svg)
        svg: str = svg.replace("switch>", "g>")

        with open(path, "wb") as fw:
            fw.write(svg.encode())

    except PermissionError:
        print(f"Недостаточно прав для записи в файл {path}")

    except RuntimeError:
        print(f"Истекло время чтения/записи файл {path}")

    except OSError as e:
        print(f"Ошибка {e.__class__.__name__}: {e.strerror}")


def process_input(files_or_dirs: Iterable[str | Path]):
    for path in files_or_dirs:
        path: Path = Path(path).expanduser().resolve()

        if not path.exists(follow_symlinks=True):
            print(f"Указан не существующий файл или директория {path}")
            continue

        elif path.is_dir():
            process_input([_.path for _ in scandir(path)])

        elif path.is_file():
            if path.suffix == ".svg":
                repair_svg(path)

            else:
                print(f"Указанный файл {path} имеет расширение не SVG")
                continue

        else:
            print(f"Не удалось обработать объект по указанному пути {path}")
            continue


if __name__ == '__main__':
    arg_parser: ArgumentParser = ArgumentParser(
        prog="repair_svg",
        usage=f"py {NAME} [ -h/--help | -v/--version ] <PATH> <PATH>",
        description="Исправление файла SVG для корректного отображения",
        epilog="",
        add_help=False,
        allow_abbrev=False,
        exit_on_error=False
    )

    arg_parser.add_argument(
        action="extend",
        type=str,
        nargs=ONE_OR_MORE,
        default=None,
        help=(
            "Путь до SVG-файла или директории. "
            "Может быть абсолютным или относительным. "
            "Может повторяться несколько раз"),
        dest="paths"
    )

    arg_parser.add_argument(
        "-v", "--version",
        action="version",
        version="1.0.0",
        help="Показать версию скрипта и завершить работу"
    )

    arg_parser.add_argument(
        "-h", "--help",
        action="help",
        default=SUPPRESS,
        help="Показать справку и завершить работу"
    )

    try:
        args: Namespace = arg_parser.parse_args()

        if hasattr(args, "help") or hasattr(args, "version"):
            input(PRESS_ENTER_KEY)
            exit(0)

    except ArgumentError as exc:
        print(f"{exc.__class__.__name__}, {exc.argument_name}\n{exc.message}")
        print("Ошибка 1")
        input(PRESS_ENTER_KEY)
        exit(1)

    except KeyboardInterrupt:
        print("Ошибка -1")
        print("Работа скрипта остановлена пользователем ...")
        exit(-1)

    else:
        paths: list[str] = getattr(args, "paths")

        process_input(paths)
