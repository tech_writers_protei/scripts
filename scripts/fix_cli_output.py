# -*- coding: utf-8 -*-
from pathlib import Path
from typing import Iterable


def get_length(line: str):
    return len(line.strip().split(maxsplit=1)[0])


def get_longest_line(lines: Iterable[str]):
    return max(map(get_length, lines))


def format_line(line: str, *, offset: int = 2, max_length: int = 0):
    name, description = line.strip().split(maxsplit=1)
    return f"{offset * ' '}{name:<{max_length}}    {description}"


def process_lines(lines: Iterable[str]):
    max_length: int = get_longest_line(lines)
    result: list[str] = [format_line(line, max_length=max_length) for line in iter(lines)]
    print("\n".join(result))


def process_from_file(file: str | Path):
    with open(file, "r", encoding="utf-8", errors="ignore") as f:
        lines: list[str] = f.readlines()

    return process_lines(lines)
