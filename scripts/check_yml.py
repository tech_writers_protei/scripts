# -*- coding: utf-8 -*-
import os.path as p
from argparse import ArgumentError, ArgumentParser, Namespace, OPTIONAL, SUPPRESS
from os import system, scandir, sep
from sys import platform

_FAIL_: str = '\033[41m'
_PASS_: str = '\033[42m'
_NORMAL_: str = '\033[0m'

_DICT_RESULTS: dict[bool, dict[str, str]] = {
    True: {
        "status": "OK",
        "color": _PASS_
    },
    False: {
        "status": "NOT OK",
        "color": _FAIL_
    }
}

STOP: str = "Нажмите любую клавишу для завершения скрипта ...\n"
NAME: str = p.basename(__file__)


def check_path(path: str):
    if not path:
        print("Ошибка 3")
        input(STOP)
        exit(3)

    abs_path: str = p.abspath(path)

    if not p.exists(abs_path):
        input("Указан путь до не существующего файла. Ошибка 4")
        exit(4)

    if not p.isfile(abs_path):
        input("Указан путь до директории, а не файла. Ошибка 5")
        exit(5)

    return abs_path


def parse():
    # specify the cli options
    arg_parser: ArgumentParser = ArgumentParser(
        prog="check_yml",
        usage=f"py {NAME} [ -h/--help | -v/--version ] <YAML_FILE> [ -o/--output <OUTPUT> ]",
        description="Валидация YAML-файла для PDF",
        epilog="",
        add_help=False,
        allow_abbrev=False,
        exit_on_error=False
    )

    arg_parser.add_argument(
        action="store",
        nargs=OPTIONAL,
        type=str,
        default=None,
        help="Путь до файла PDF_*.yml",
        dest="yaml_file"
    )

    arg_parser.add_argument(
        "-o", "--output",
        action="store",
        default=SUPPRESS,
        required=False,
        help="Файл для записи вывода",
        dest="output"
    )

    arg_parser.add_argument(
        "-v", "--version",
        action="version",
        version="1.0.1",
        help="Показать версию скрипта и завершить работу"
    )

    arg_parser.add_argument(
        "-h", "--help",
        action="help",
        default=SUPPRESS,
        help="Показать справку и завершить работу"
    )

    # parse the user input
    try:
        args: Namespace = arg_parser.parse_args()

        if hasattr(args, "help") or hasattr(args, "version"):
            input(STOP)
            exit(0)

    except ArgumentError as exc:
        print(f"{exc.__class__.__name__}, {exc.argument_name}\n{exc.message}")
        print("Ошибка 1")
        input(STOP)
        exit(1)

    except KeyboardInterrupt:
        print("Ошибка 2")
        input("Работа скрипта остановлена пользователем ...")
        exit(2)

    else:
        return args


def validate(yaml_file: str | None, out: str | None):
    if yaml_file is None:
        for file in scandir(p.curdir):
            name, extension = p.splitext(file.name)

            if extension == ".yml" and name.startswith("PDF"):
                yaml_file: str = file.name
                print(f"Обнаружен файл {yaml_file}\n")
                break

        else:
            print("Не найден YAML-файл для чтения")
            user_input: str = input("Укажите путь до файла:\n\n")

            yaml_file: str = check_path(user_input)

    if platform.startswith("win"):
        system("color")

    _dirname: str = p.dirname(yaml_file)

    with open(yaml_file, "r", errors="ignore") as f:
        paths: dict[str, int] = {
            p.join(p.dirname(yaml_file), line.strip().removeprefix("- ")): index + 1
            for index, line in enumerate(f.readlines())
            if line.strip().startswith("-")}

    max_length: int = max(map(len, paths)) + 10
    _lines: list[str] = []

    __is_ok: bool = True

    for path, line_no in paths.items():
        _result: bool = p.exists(path)

        if not _result and __is_ok:
            __is_ok = False

        _status: str = _DICT_RESULTS.get(_result).get("status")
        _color: str = _DICT_RESULTS.get(_result).get("color")

        _path: str = p.relpath(path, _dirname).replace(sep, "/")

        _line = f"{_color}{line_no:>3}  {_path:.<{max_length}}{_status:.>6}{_NORMAL_}"
        _lines.append(_line)

        print(_line)

    if out is not None and out:
        out: str = p.normpath(p.abspath(out))
        mode: str = "w" if p.exists(out) else "x"

        with open(out, mode) as f:
            f.write(
                "\n".join(_lines).
                replace(_PASS_, "").
                replace(_FAIL_, "").
                replace(_NORMAL_, ""))

    return


if __name__ == '__main__':
    ns: Namespace = parse()
    yaml: str = getattr(ns, "yaml_file", None)
    output: str | None = getattr(ns, "output", None)
    validate(yaml, output)
