# -*- coding: utf-8 -*-
from glob import iglob
from pathlib import Path
from re import DOTALL, MULTILINE, Pattern, compile, finditer
from typing import Iterable

_PATTERN_HTML: Pattern = compile(r"<(\S+)\s?[^>]*>.*?</(\S+)>", DOTALL)
_PATTERN_CODE: Pattern = compile(r"```(.*?)\n```", DOTALL | MULTILINE)


def check_directory(path: str | Path, recursive: bool = True):
    path: Path = Path(path).resolve()
    storage: list[str] = []

    for file in iglob("**/*", root_dir=path, recursive=recursive):
        _: Path = path.joinpath(file)

        with open(_, "r", encoding="utf-8", errors="ignore") as f:
            _content: str = f.read()

        for index, line in enumerate(_content):
            for m in finditer(_PATTERN_HTML, line):
                _m_start: str = m.group(1)
                _m_end: str = m.group(2)

                if m.group(1) != m.group(2):
                    storage.append(
                        f"Файл {_.resolve()}: Строка {index + 1}: "
                        f"открыт тег {_m_start}, закрыт тег {_m_end}")

    if not storage:
        return "Все теги корректны"

    else:
        return f"Найденные ошибки:\n{'\n'.join(storage)}"
