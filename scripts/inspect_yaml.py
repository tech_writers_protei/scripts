# -*- coding: utf-8 -*-
import os.path as p
from os import system, scandir, sep
from sys import platform


def validate_file(yaml_file: str | None, log_file: str):
    _FAIL_: str = '\033[41m'
    _PASS_: str = '\033[42m'
    _NORMAL_: str = '\033[0m'

    _DICT_RESULTS: dict[bool, dict[str, str]] = {
        True: {
            "status": "OK",
            "color": _PASS_
        },
        False: {
            "status": "NOT OK",
            "color": _FAIL_
        }
    }

    __is_ok: bool = True

    if yaml_file is None:
        return __is_ok

    _dirname: str = p.dirname(yaml_file)

    with open(yaml_file, "r", encoding="utf-8", errors="ignore") as f:
        paths: dict[str, int] = {
            p.join(_dirname, line.strip().removeprefix("- ")): index + 1
            for index, line in enumerate(f.readlines())
            if line.strip().startswith("-")}

    max_length: int = max(map(len, paths)) + 10
    _lines: list[str] = []

    for path, line_no in paths.items():
        _result: bool = p.exists(path)

        if not _result and __is_ok:
            __is_ok = False

        _status: str = _DICT_RESULTS.get(_result).get("status")
        _color: str = _DICT_RESULTS.get(_result).get("color")

        _path: str = p.relpath(path, _dirname).replace(sep, "/")

        _line = f"{_color}{line_no:>3}  {_path:.<{max_length}}{_status:.>6}{_NORMAL_}"
        _lines.append(_line)

    if not __is_ok:
        out: str = p.normpath(p.abspath(p.join(p.curdir, log_file)))
        mode: str = "w" if p.exists(out) else "x"

        with open(out, mode) as f:
            f.write(
                "\n".join(_lines).
                replace(_PASS_, "").
                replace(_FAIL_, "").
                replace(_NORMAL_, ""))

        print(
            f"Файл {yaml_file} некорректен, вывод команды записан в файл {out}"
            .encode("utf-8").decode("utf-8"))

    return __is_ok


def validate():
    __final_status: bool = True

    yaml_files: list[str] = []

    for file in scandir(p.curdir):
        name, extension = p.splitext(file.name)

        if extension == ".yml" and name.startswith("PDF"):
            yaml_files.append(file.name)

    if not yaml_files:
        print("Файлы YAML не найдены".encode("utf-8").decode("cp1251"))
        raise SystemExit(not __final_status)

    if platform.startswith("win"):
        system("color")

    __flags: list[bool] = []

    for yaml_file in yaml_files:
        _: str = yaml_file.replace(".", "_")
        log_file: str = f"inspect_yaml_{_}.log"
        _flag: bool = validate_file(yaml_file, log_file)
        __flags.append(_flag)

    __final_status = all(__flags)

    raise SystemExit(not __final_status)


if __name__ == '__main__':
    validate()
