# -*- coding: utf-8 -*-
from argparse import ArgumentError, ArgumentParser, Namespace, SUPPRESS
from glob import iglob
from pathlib import Path

PRESS_ENTER_KEY: str = "\nНажмите ENTER, чтобы завершить работу скрипта ..."
RUSSIAN_CHARS: str = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
NAME: str = Path(__file__).name

_FAIL_: str = '\033[41m'
_PASS_: str = '\033[42m'
_NORMAL_: str = '\033[0m'

if __name__ == '__main__':
    arg_parser: ArgumentParser = ArgumentParser(
        prog="validate_yaml_file",
        usage=f"py {NAME} [ -h/--help | -v/--version ] <PATH> [ --no-recursive ]",
        description="Проверка наличия не переведенных слов",
        epilog="",
        add_help=False,
        allow_abbrev=False,
        exit_on_error=False
    )

    arg_parser.add_argument(
        action="store",
        type=str,
        default=None,
        help="Путь до файла или директории",
        dest="path"
    )

    arg_parser.add_argument(
        "--no-recursive",
        action="store_false",
        default=SUPPRESS,
        required=False,
        help="Флаг отключения рекурсивного поиска файлов в подпапках.\nЕсли указан путь до файла, то игнорируется",
        dest="recursive"
    )

    arg_parser.add_argument(
        "-v", "--version",
        action="version",
        version="1.0.0",
        help="Показать версию скрипта и завершить работу"
    )

    arg_parser.add_argument(
        "-h", "--help",
        action="help",
        default=SUPPRESS,
        help="Показать справку и завершить работу"
    )

    try:
        args: Namespace = arg_parser.parse_args()

        if hasattr(args, "help") or hasattr(args, "version"):
            input(PRESS_ENTER_KEY)
            exit(0)

    except ArgumentError as e:
        print(f"{e.__class__.__name__}, {e.argument_name}\n{e.message}")
        print("Ошибка 1")
        input(PRESS_ENTER_KEY)
        exit(1)

    except KeyboardInterrupt:
        print("Ошибка 2")
        print("Работа скрипта остановлена пользователем ...")
        exit(2)

    else:
        path: str = getattr(args, "path")
        recursive: bool = getattr(args, "recursive", True)

    result: list[str] = []

    path: Path = Path(path).expanduser().resolve()

    if not path.exists():
        print("Указан не существующий путь")
        print("Ошибка 3")
        raise FileNotFoundError

    if path.is_file():
        files: list[Path] = [path]

    elif path.is_dir():
        if recursive:
            files: list[Path] = [
                path.joinpath(item) for item in
                iglob("**/*.en.*", root_dir=path, recursive=recursive, include_hidden=False)]
        else:
            files: list[Path] = [item for item in path.iterdir() if ".en" in item.suffixes]

    else:
        print("Указан некорректный путь")
        print("Ошибка 4")
        raise NotADirectoryError

    for file in files:
        _indexes: list[str] = []

        with open(file, "r", encoding="utf-8", errors="ignore") as f:
            _indexes.extend(
                f"{index + 1}" for index, line in enumerate(f.readlines())
                if any(char in line for char in RUSSIAN_CHARS))

        if not _indexes:
            result.append(f"{_PASS_}В файле {file.relative_to(path.parent)} не найдены кириллические буквы{_NORMAL_}")
        else:
            result.append(
                f"{_FAIL_}В файле {file.relative_to(path.parent)} найдены кириллические буквы в строках: "
                f"{', '.join(_indexes)}{_NORMAL_}")

    print("\n".join(result))

    input(PRESS_ENTER_KEY)
    exit(0)
