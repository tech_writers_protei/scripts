# -*- coding: utf-8 -*-
from os.path import getsize
from pathlib import Path

from PIL import Image

if __name__ == '__main__':
    for file in Path(r"C:\Users\tarasov-a\PycharmProjects\pdf_builder\PDF_Build\images").iterdir():
        if file.suffix == ".png":
            old_size: int = getsize(file)

            with Image.open(file) as img:
                img.save(img.filename, format="PNG", optimize=True)

            new_size: int = getsize(file)

            print(f"File {file}: {old_size} -> {new_size}")

        else:
            continue

    print("-" * 80)
