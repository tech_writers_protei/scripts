# -*- coding: utf-8 -*-
from io import UnsupportedOperation
from pathlib import Path
from typing import Iterable

from loguru import logger


def write_file(file_path: str | Path, content: str | Iterable[str]):
    if isinstance(content, Iterable):
        content: str = "\n".join(content)

    try:
        with open(file_path, "w") as f:
            f.write(content)
            logger.debug(f"File {file_path} is written")

    except FileNotFoundError:
        logger.error(f"Файл {file_path} не найден")
        raise

    except PermissionError:
        logger.error(f"Недостаточно прав для записи в файл {file_path}")
        raise

    except RuntimeError:
        logger.error(f"Истекло время записи в файл {file_path}")
        raise

    except OSError as e:
        logger.debug(f"Ошибка {e.__class__.__name__}: {e.strerror}")
        raise


def read_file(file_path: str | Path) -> str:
    try:
        with open(file_path, "r") as f:
            _: str = f.read()

    except FileNotFoundError:
        logger.error(f"Файл {file_path} не найден")
        raise

    except PermissionError:
        logger.error(f"Недостаточно прав для записи в файл {file_path}")
        raise

    except RuntimeError:
        logger.error(f"Истекло время записи в файл {file_path}")
        raise

    except UnsupportedOperation:
        logger.error(f"Не поддерживаемая операция с файлом {file_path}")
        raise

    except OSError as e:
        logger.error(f"Ошибка {e.__class__.__name__}: {e.strerror}")
        raise

    else:
        logger.debug(f"Файл {file_path} прочитан")
        return _


def read_lines_file(file_path: str | Path) -> list[str]:
    try:
        with open(file_path, "r") as f:
            _: list[str] = f.readlines()

    except FileNotFoundError:
        logger.error(f"Файл {file_path} не найден")
        raise

    except PermissionError:
        logger.error(f"Недостаточно прав для записи в файл {file_path}")
        raise

    except RuntimeError:
        logger.error(f"Истекло время записи в файл {file_path}")
        raise

    except OSError as e:
        logger.error(f"Ошибка {e.__class__.__name__}: {e.strerror}")
        raise

    else:
        logger.debug(f"File {file_path} is read")
        return _


def check_path_file(path: str | Path):
    path: Path = Path(path).expanduser().resolve()

    if not path.exists():
        logger.error(f"Путь {path} не существует")
        raise FileNotFoundError

    if not path.is_file():
        logger.error(f"Путь {path} указывает не на директорию")
        raise IsADirectoryError

    return


def check_path_directory(path: str | Path):
    path: Path = Path(path).expanduser().resolve()

    if not path.exists():
        logger.error(f"Путь {path} не существует")
        raise FileNotFoundError

    if not path.is_dir():
        logger.error(f"Путь {path} указывает не на директорию")
        raise NotADirectoryError

    return
